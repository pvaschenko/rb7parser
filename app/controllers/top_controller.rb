class TopController < ApplicationController
	include ApplicationHelper

	def index
		self.get_movies
		@movies = Movies.order("sessions DESC").limit(3)
	end
end