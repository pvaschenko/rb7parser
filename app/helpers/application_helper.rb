module ApplicationHelper
  require 'open-uri'

  def get_movies
    url = 'https://rb7.ru/afisha/movies'
    html = open(url)
    doc = Nokogiri::HTML(html)
    movies = []
    doc.css('.movie').each do |movie|
      sessions = movie.css('.show').map { |session| session.text.strip }
      title_el = movie.at_css('div.data h2 a')
      title = title_el.text.strip
      movies.push(
          title: title,
          sessions: sessions.length
      )
    end
    if (movies.length)
      Movies.delete_all
      movies.each do |movie|
        mov = Movies.new(name: movie[:title], sessions: movie[:sessions])
        mov.save
      end
    end
  end
end
